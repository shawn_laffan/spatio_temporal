df2asc = function (tdata, lat_colname = "Y", lon_colname = "X", cellsize = NULL,  filenames = NULL, outdir = getwd(), gz = FALSE) 
{
    if (is.null(filenames)) {
        filenames = colnames(tdata)[3:length(tdata)]
    }
    else {
        if (length(filenames) != length(3:length(tdata))) 
            stop("variable names must be the same length as the files vector")
        filenames = as.character(filenames)
    }
    
    library(SDMTools)
    
    lat_colnum = which(names(tdata)==lat_colname)
    lon_colnum = which(names(tdata)==lon_colname)
    
    lats = unique(tdata[[lat_colnum]])
    lats = sort(lats)
    longs = unique(tdata[[lon_colnum]])
    longs = sort(longs)
    if (is.null(cellsize)) {
      cellsize = min(c(diff(lats), diff(longs)))
    }
    nc = ceiling((max(lats) - min(lats))/cellsize) + 1
    nr = ceiling((max(longs) - min(longs))/cellsize) + 1
    if (nr == 1) {nr = 2}  #  a function below doesn't like rep=1
    if (nc == 1) {nc = 2}
#print (paste (c("nr=", nr, "nc=", nc, "cellsize=", cellsize)))
    for (ii in 4:(length(tdata))) {
        #print (paste (cellsize, nr, nc, min(longs), min(lats)))
        out.asc = as.asc(matrix(NA, nrow = nr, ncol = nc), xll = min(longs), 
            yll = min(lats), cellsize = cellsize)
        out.asc = put.data(tdata[, c(lon_colnum, lat_colnum, ii)], out.asc)
        write.asc(out.asc, paste(outdir, "/", filenames[ii - 
            2], sep = ""), gz = gz)
    }
}


combined_csv2df = function (globber, ...) {
  if (is.null(globber)) { 
    stop("glob argument is NULL")
  }
  
  files = Sys.glob(globber)
  
  if (length(files) == 0) {
    stop ("no files found")
  }
  
  print (paste ("processing", length(files), "files"))
  
  df = data.frame()
  for (file in files) {
    data = read.csv (file, ...)
    df = rbind (df, data)
  }
  return (df)
}

folder_csv2df = function (globber, folder) {
  if (is.null(globber)) { 
    stop("glob argument is NULL")
  }
  if (is.null(folder)) { 
    folder = getwd()
  }
  glob = paste (folder, globber, sep="")
  df = combined_csv2df (glob)
  return (df)
}

allfolders_csv2df = function (fileglobber, folderglobber) {
  if (is.null(fileglobber)) { 
    stop("fileglobber argument is NULL")
  }
  if (is.null(folderglobber)) { 
    folderglobber = getwd()
  }
  
  folders = Sys.glob(folderglobber)
  
  df = data.frame()
  
  for (folder in folders) {
    glob = paste (folder, fileglobber, sep="/")
    print (glob)
    df2 = combined_csv2df (glob)
    df = rbind (df, df2)
  }
  return (df)
}

#  df_trend = allfolders_csv2df ("*trend*", "g:/shared/laura_data/results/tile*")

