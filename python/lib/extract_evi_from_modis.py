"""
Extract the EVI or NDVI layers from a MODIS hdf tile
Don't use this script for multiple files.
Use mosaic_modis3.py instead.
"""


# Import system modules
import sys, string, os
import arcpy
import arcpy.management as arcmgt
from arcpy.sa import *

bands = {
    "NDVI": 0,
    "EVI": 1,
    "VI Quality": 2,
    "red": 3,
    "NIR": 4,
    "blue": 5,
    "MIR": 6,
    "view_zenith": 7,
    "sun_zenith": 8,
    "rel_azimuth": 9,
    "day_of_yr": 10,
    "reliability": 11,
}

if __name__ == "__main__":
    globber     = arcpy.GetParameterAsText (0)
    target_name = arcpy.GetParameterAsText (1)
    src_wkspace = arcpy.GetParameterAsText (2)
    bounds      = arcpy.GetParameterAsText (3)

    start_cwd = os.getcwd()

    start_wkspace = arcpy.env.workspace
    if (arcpy.env.workspace is None):
        arcpy.env.workspace = start_cwd
        start_wkspace = arcpy.env.workspace

    if len(src_wkspace):
        arcpy.env.workspace = src_wkspace
    if (arcpy.env.workspace is None):
        arcpy.env.workspace = start_cwd

    if len(globber) == 0:
        globber = "*.hdf"

    rasters = arcpy.ListRasters (globber)


    if len (target_name) == 0 or target_name == "#":
        target_name = "EVI"
    target = bands[target_name]

    if len(bounds) == 0 or bounds == "#":
        bounds = "MAXOF"

    arcpy.env.extent     = bounds
    arcpy.env.snapRaster = rasters[0]
    arcpy.env.overwriteOutput = True
    arcpy.CheckOutExtension("Spatial")
    
    print arcpy.env.extent
    
    for i in range (len(rasters)):
        try:
            name = "%s_%s.img" % (target_name, rasters[i][8:16])
            print "Processing %s" % name
            if arcpy.Exists(name):
                print "Output exists, skipping"
                continue

            arcmgt.ExtractSubDataset(rasters[i], name, 1)

        except Exception as e:
            print name
            print e.message

    print "Finished"
