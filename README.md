A set of perl, R and python scripts to download and analyse spatio-temporal satellite imagery.  

This is primarily MODIS.

Note that the download script might no longer work, as it hard codes the target URLs and file structure.  These have been known to change in the past, but have not been checked for some time.  

Analysis results using the BFAST algorithm are described in Watts and Laffan (2014) http://dx.doi.org/10.1016/j.rse.2014.08.023

Note that the image sampling and map generation can now be largely done within R using the geospatial libraries (rgdal for example).  
