#set observed and randomisations biodiverse file
input_file <- choose.files(caption="Select the results .csv file")
out_dir <- choose.dir(caption="Select the output folder")

#  must have loaded the df2asc function from df2asc.R

data = read.csv (input_file , header=T, sep=",")

data = cbind (Y = data$Y, X = data$X, data[,4:length(colnames(data))])

df2asc (data, filenames = NULL, outdir = out_dir, gz = FALSE) #output the dataframe to an asc file

