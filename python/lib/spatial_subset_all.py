#!/usr/bin/env python

"""
Extract geographic subsets for all rasters.
Retains the same names, but puts them in a different folder.
"""

# Import system modules
import sys, string, os
#import re
import arcpy
import arcpy.management as arcmgt
from arcpy.sa import Plus

if __name__ == "__main__":
    target_names  = arcpy.GetParameterAsText (0)
    out_workspace = arcpy.GetParameterAsText (1)
    extent        = arcpy.GetParameterAsText (2)


    if arcpy.env.workspace is None:
        arcpy.env.workspace = os.getcwd()
        workspace = arcpy.env.workspace

    if len(target_names) == 0:
        raise "No targets specified"
    
    arcpy.CheckOutExtension("Spatial")
    
    rasters = []
    for glob in string.split(target_names, ";"):
        print "glob: %s" % glob
        raster_list = sorted (arcpy.ListRasters (glob))
        rasters.extend(raster_list)
    rasters_as_string = string.join (rasters, ";")
    
    if extent != '#' and len (extent) > 0:
        arcpy.env.extent = extent
    print "Extent is %s" % arcpy.env.extent

    for raster in rasters:
        try:
            name = os.path.join (out_workspace, os.path.basename (raster))
    
            print "Processing %s" % name
            if arcpy.Exists(name):
                print "Output exists, skipping"
            
            result = Plus (raster, 0)
            result.save(name)

        except Exception as e:
            print arcpy.GetMessages()
            raise
