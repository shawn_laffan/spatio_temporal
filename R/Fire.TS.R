#setwd()
library("lubridate", lib.loc="D:/laura/R_libs")

source('D:/laura/spatio_temporal/R/BFAST Functions.R', echo=TRUE)

create16dayts <- function(data,dates) {
  z <- zoo(data,dates)
  yr <- as.numeric(format(time(z), "%Y"))
  jul <- as.numeric(format(time(z), "%j"))
  delta <- min(unlist(tapply(jul, yr, diff))) # 16
  zz <- aggregate(z, yr + (jul - 1) / delta / 23)
  (tso <- as.ts(zz))
  return(tso)  
}

EVIfire2000_2002 = read.csv("EVIfire_2000_2002.txt", row.names = 2)
EVIfire2003_2005 = read.csv("EVIfire_2003_2005.txt", row.names = 2)
EVIfire2006_2008 = read.csv("EVIfire_2006_2008.txt", row.names = 2)
EVIfire2009_2012 = read.csv("EVIfire_2009_2012.txt", row.names = 2)


startcol = 4

zz0002 = length(colnames(EVIfire2000_2002))
zz0305 = length(colnames(EVIfire2003_2005))
zz0608 = length(colnames(EVIfire2006_2008))
zz0912 = length(colnames(EVIfire2009_2012))

EVI_data = t(cbind (EVIfire2000_2002[,startcol:zz0002], EVIfire2003_2005[,startcol:zz0305], EVIfire2006_2008[,startcol:zz0608],EVIfire2009_2012[,startcol:zz0912]))

EVI_dates = as.Date(rownames(EVI_data), "EVI_A%Y%j")

fire2000_2001 = read.csv("fire_2000_2001.txt", row.names = 2)
fire2002_2003 = read.csv("fire_2002_2003.txt", row.names = 2)
fire2004_2005 = read.csv("fire_2004_2005.txt", row.names = 2)
fire2006_2007 = read.csv("fire_2006_2007.txt", row.names = 2)
fire2008_2009 = read.csv("fire_2008_2009.txt", row.names = 2)
fire2010_2011 = read.csv("fire_2010_2011.txt", row.names = 2)
fire2012 = read.csv("fire_2012.txt", row.names = 2)


startcol = 4

zz0001 = length(colnames(fire2000_2001))
zz0203 = length(colnames(fire2002_2003))
zz0405 = length(colnames(fire2004_2005))
zz0607 = length(colnames(fire2006_2007))
zz0809 = length(colnames(fire2008_2009))
zz1011 = length(colnames(fire2010_2011))
zz12 = length(colnames(fire2012))

fire_data = t(cbind (fire2000_2001[,startcol:zz0001], fire2002_2003[,startcol:zz0203], fire2004_2005[,startcol:zz0405],fire2006_2007[,startcol:zz0607],fire2008_2009[,startcol:zz0809], fire2010_2011[,startcol:zz1011],fire2012[,startcol:zz12]))

fire_dates = as.Date(rownames(fire_data), "fire_A%Y%j")


t16_evi = list ()
for (i in 1:length(colnames(EVI_data))) {
  t16_evi[[i]] = create16dayts (EVI_data[,i], EVI_dates)
}

ts_fire = list()
for (i in 1:length (colnames(fire_data))) {
  ts_fire[[i]] = ts (fire_data[,i],start = c(2000,8), end = c(2012, 46), frequency = 46)
}

for (i in 1:length (ts)) {plot (ts[[i]], ylab = "fire value", main = paste ("Sample", i))}
for (i in 1:60) {plot (ts[[i]], ylab = "fire value", main = paste ("Sample", i)); dev.off()}

bf  = list()
pscores = list()
breakdates = list()
magnitudes = list()
seasonal_breakdates = list()
ci = list()
h_param = 1/5

for (i in 1:length(colnames(EVI_data))) {
  print (paste ("sample ", i, sep=""))
  bf[[i]] = bfast (t16_evi[[i]], h = h_param, season = "dummy", max.iter = 10, breaks = NULL)
  pscores[[i]] = bf_get_p (bf[[i]])
  breakdates [[i]] = get_trend_breakdates (bf [[i]])
  seasonal_breakdates [[i]] = get_seasonal_breakdates (bf[[i]])
  magnitudes [[i]] = bf[[i]]$Magnitude
  ci[[i]] = bf_get_ci (bf[[i]])
}

capture.output(pscores, file = "pscores.txt", append = TRUE)
capture.output(breakdates, file = "breakdates2.txt", append = TRUE)
capture.output (magnitudes, file = "magnitudes.txt", append = TRUE)
capture.output (dw_test, file = "autocorrelation.txt", append = TRUE)

capture.output (ci, file = "Conf_Intervals.csv", append = TRUE)
capture.output(seasonal_breakdates, file = "seasonal_breakdates.csv", append = TRUE)


for (i in 1:60) {bmp(paste("evifire_",i,".bmp",sep="")); plot (bf[[i]], ANOVA=T, main = paste("Sample", i, "EVI")); dev.off()}
for (i in 1:60) {png(paste("evifire_",i,".png",sep="")); plot (bf[[i]], ANOVA=T, main = paste("Sample", i, "EVI")); dev.off()}
for (i in 1:60) {png(paste("evifire__noise",i,".png",sep="")); plot (bf[[i]],type ="noise",ylab = "Remainder Component", main = paste("Sample", i, "EVI")); dev.off()}

#Subset value >5
for (i in 1:60) {print (ts[[i]][ts[[i]] > 5])}

fires = read.table ("fires.csv.", sep = ",", header = T)
firedates = fires$Date

decimal_date <- function(date){
  
  decimal <- as.numeric(difftime(date, floor_date(date, "year"), units = "secs"))
  if (decimal != 0) {
    decimal <- decimal / as.numeric(
      difftime(ceiling_date(date,"year"), 
               floor_date(date, "year"), 
               units = "secs"))
  }
  
  year(date) + decimal
}



decimal_datex <- function(date){
  
  decimal <- as.numeric(difftime(date, floor_date(date, "year"), units = "secs"))
  nonzero <- decimal != 0
  #print (decimal)
  #print (nonzero)
  
  if (sum (nonzero)) {
    decimal[nonzero] <- decimal[nonzero] / as.numeric(
      difftime(ceiling_date(date[nonzero],"year"), 
               floor_date(date[nonzero], "year"), 
               units = "secs"))
    
  }
  decimal[!nonzero] = 0
  
  year(date) + decimal
}

decimal_datex (as.Date ("2000001", "%Y%j"))
decimal_datex (as.Date ("2000002", "%Y%j"))

fire_dates_decimal = decimal_datex (as.Date (names(ts_fire[[i]]), "fire_A%Y%j"))

#  need to rename t16 to t16_EVIfire, and ts to fire_ts
par (mfrow = c(6,10))


for (row_id in 1:60) {
  png(paste("fire_evi",row_id,".png",sep=""));
  plot (t16_evi[[row_id]],ylab = "EVI", main = paste("Sample", row_id))
  for (i in fire_dates_decimal[ts_fire[[row_id]] == 7]) {
    abline (v = i, col = "blue")
  }
  for (i in fire_dates_decimal[ts_fire[[row_id]] == 8]) {
    abline (v=i, col = "dark orange")
  }
  for (i in fire_dates_decimal[ts_fire[[row_id]] == 9]) {
    abline (v=i, col = "red")
  };dev.off()
}


for (sample_no in 1:60){
  print (fire_dates_decimal[ts_fire[[sample_no]]>6])
  print(sample_no)
}

for (sample_no in 1:60){
  print (ts_fire[[sample_no]][ts_fire[[sample_no]]>6])
  print(sample_no)
}

decimal_datex <- function(date){
  
  decimal <- as.numeric(difftime(date, floor_date(date, "year"), units = "secs"))
  nonzero <- decimal != 0
  #print (decimal)
  #print (nonzero)
  
  if (sum (nonzero)) {
    decimal[nonzero] <- decimal[nonzero] / as.numeric(
      difftime(ceiling_date(date[nonzero],"year"), 
               floor_date(date[nonzero], "year"), 
               units = "secs"))
    
  }
  decimal[!nonzero] = 0
  
  year(date) + decimal
}