import arcpy
import numpy as np
import string
import os

from arcpy import env
#from arcpy.sa import Pick

arcpy.CheckOutExtension("Spatial")

class NoRasters(Exception):
    pass

env.overwriteOutput = True
env.workspace = r"D:\laura\all_rasters"
if not os.path.exists(env.workspace):
    env.workspace = os.getcwd()
out_pfx = 'ps'

#  all rasters must be of the same extent
t_rasterList = arcpy.ListRasters ('Trend_Breakdate_*.asc')
p_rasterList = arcpy.ListRasters ('P_Value_*.asc')

if not t_rasterList:
    raise NoRasters ("No rasters match trend query")

if not p_rasterList:
    raise NoRasters ("No rasters match p-score query")

print p_rasterList
print t_rasterList


t_np_rasters = []
for raster_name in t_rasterList:
    this_ras = arcpy.RasterToNumPyArray(raster_name)  #  read this raster
    t_np_rasters.append(this_ras)

n_timesteps = 296
#n_timesteps = 50 #### DEBUG DEBUG
name_fmt = "%s_%0" + str(len(str(n_timesteps))) + "i.img"  #  allow for differing sequence lengths

max_timestep = t_np_rasters[-1].max()

#  numpy array storing the value of which period we are in
#  np.ones_like does not work with conversion to raster, not sure why
period_ras = np.zeros_like(t_np_rasters[0])


desc = arcpy.Describe(t_rasterList[-1])
nodata = desc.noDataValue
cellsize = desc.meanCellWidth
ll_pt = arcpy.Point (desc.extent.XMin, desc.extent.YMin)

#  ugly hack as Arc does not like np.ones_like() or np.zeros_like()+1
period_ras = np.where (period_ras == 0, 1, period_ras)


for timestep in range (1, n_timesteps+1):
    
    for this_ras in t_np_rasters:
        #print timestep, this_ras.min(), this_ras.max()
        period_ras = np.where (this_ras == timestep, period_ras + 1, period_ras)

    print period_ras.min(), period_ras.max()

    t_ras = arcpy.NumPyArrayToRaster (period_ras, ll_pt, cellsize)

    p_pd_ras = arcpy.sa.Pick (t_ras, p_rasterList)

    out_name = name_fmt % (out_pfx, timestep)
    print out_name
    p_pd_ras.save(out_name)

