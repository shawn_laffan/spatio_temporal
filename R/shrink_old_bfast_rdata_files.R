args = commandArgs(TRUE)


if (!length(args) | is.na (args[1])) {
  print ("Missing file argument")
  q()
}

#args = c("bfast_res_07_")

print (args[1])
print (getwd())

globber = paste (args[1], "*.RData", sep="")


files = Sys.glob(globber)

print (paste ("processing", length (files), "files"))


#files = head(files)

tmp_obj = list(a = 1:10)


for (file in files) {
  if (file.exists (file)) {
    info = file.info (file) 
    size = info$size
    newname = paste (file, ".xx", sep="")

    if (size > 20000000 && ! (file.exists (newname))) {      
      save(tmp_obj, file=newname)
      print (paste (file, size))
      load (file, .GlobalEnv)
      print (paste (ls()))
      if (exists ("results")) {
        
        if (1) {
          print (newname)
          file.rename(file, newname)
          #print (names (results))
          bf = results$bf
          #print (paste (names (bf)))
          save (bf, file = file, compress = "xz")
        } else {
          print (paste (newname, "exists, skipping"))
        }
      } else {
        print ("FAIL")
      }
      rm ("results")
    }
  }
}

#load(file, envir = parent.frame())

