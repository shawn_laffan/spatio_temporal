args = commandArgs(TRUE)
#print (args)
if (!length(args) | is.na (args[1])) {
  print ("Missing file argument")
  q()
}

fname = args[1]
# start_row = as.numeric(ifelse (!is.na (args[2]), args[2], 1))
# end_row   = as.numeric(ifelse (!is.na (args[3]), args[3], 5))
start_row = 1
end_row   = 10**100

default_wk = "/projects/ed4/test_data"
default_wk = getwd()
workspace  = ifelse (is.na (args[2]), default_wk, args[2])
overwrite  = ifelse (is.na (args[3]), FALSE, args[3])

print (paste ("file is", fname))
print (paste ("start_row and end_row are", start_row, end_row))
print (paste ("workspace is", workspace))
setwd(workspace)

library (bfast)
library (lmtest)

bfast_fns_file = Sys.getenv ("BFAST_FNS_FILE")
if (is.na(bfast_fns_file)) {bfast_fns_file = "/projects/ed4/repos/spatio_temporal/R/bfast_fns.R"}
print (paste ("Loading functions from", bfast_fns_file))
source (bfast_fns_file, echo=FALSE)

bfast_use_hpc = Sys.getenv ("BFAST_USE_HPC")
if (bfast_use_hpc != "" & bfast_use_hpc != "0") {
  print ("Bfast using HPC options")
  library(foreach)
  library(doMC)
  registerDoMC(as.numeric(bfast_use_hpc))
} else {
  print ("Bfast not using HPC options")
  bfast_use_hpc = NULL
}


data = read.csv (fname, row.names = 1)
end_row = min (end_row, length(rownames(data)))
data = data[start_row:end_row,]
print (paste ("Processing ", length(rownames(data)), "records"))

startcol = 3
ncols = length(colnames(data))

#print (paste ("len1 is ", len1))

tsdata = t(data[,startcol:ncols])

coords = data[,1:2]

colnames(tsdata) = start_row:end_row


flen = nchar(fname)
offset = flen-5; #  gets 01.csv
i = substr (fname, offset, offset+1)
fstart_row = sprintf ("%06i", start_row)
fend_row   = sprintf ("%06i", end_row)
file_pfx   = paste ("bfast_res", i, fstart_row, fend_row, sep = "_")
print (paste ("file prefix is", file_pfx))

system.time (
  (bfast_results = run_bf_for_df_subsets(
    tsdata, 
    coords = coords, 
    file_pfx = file_pfx, 
    overwrite = overwrite,
	end_row = end_row,
    seq_by = 10
  ))
)

