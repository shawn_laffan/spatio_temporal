import arcpy
import string
import os
import time
import datetime
import re


def get_date_string_from_name (name):
    dt = time.strptime (name, "d%Y.%m.%d.img")
    date = time.strftime("%d-%b-%Y",dt)
    return date

folder = "D:/laura/trend_rasters"
folder = r"D:\shawn\scratch\laura\plot_trend_rasters\trend_rasters"
arcpy.env.workspace = folder
glob = "d*.img"

bmp_folder = os.path.join (folder, "bmp_trends")
try:
    os.mkdir (bmp_folder)
except:
    pass

mxd_file = "D:/laura/Maps/trends_to_BMP.mxd"
mxd_file = r"D:\shawn\scratch\laura\plot_trend_rasters\trends_to_BMP_10.1.mxd"
mxd = arcpy.mapping.MapDocument(mxd_file)

arcpy.env.workspace = folder
rasterList = arcpy.ListRasters (glob)
print (rasterList)
df =  arcpy.mapping.ListDataFrames(mxd, "Trends")[0]

lyr = None
for lyr_i in arcpy.mapping.ListLayers(mxd):
    if lyr_i.name == "Trend Slope":  ###
        lyr = lyr_i
        continue

#symType = lyr.symbologyType
#sym = lyr.symbology

text_elt_prop_name = None
for elt in arcpy.mapping.ListLayoutElements (mxd, "TEXT_ELEMENT"):
    if elt.name == "map_title":
        text_elt_prop_name = elt
        break


print (lyr)

i = -1
for raster in rasterList:
    #  only do a subset of images
    i = i + 1
    #p_raster = p_raster_list[i]

    #if i == 6:
        #break

    try:
        print raster
        lyr.replaceDataSource(folder, "RASTER_WORKSPACE", raster)
    except Exception as e:
        raise e

    out_img = os.path.join (bmp_folder, string.replace(raster, "img", "bmp"))
    if os.path.isfile(out_img):
        print "Skipping %s, file exists" % out_img
        continue
    
    print "Saving %s as %s" % (raster, out_img)
    try:
        name = get_date_string_from_name(raster)
        print "Name is %s" % name
        #df.name = name
        text_elt_prop_name.text = name

        arcpy.mapping.ExportToBMP(mxd, out_img)
    except Exception as e:
        raise e

#  A little cleanup.
#  Might stop the problem of being unable to edit 
#   and save the map document.
try:
    del (mxd, df, lyr)
except:
    pass

print "completed"

