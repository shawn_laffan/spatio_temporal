import arcpy
import string
import os
import time
import re

re_num = re.compile("(\d{7})")


def get_date_string (date):
    dt = time.strptime(date, "%Y%j")
    #date = "%s-%s-%s" % (dt.tm_year, dt.tm_month, dt.tm_day)
    #date = time.asctime(dt)
    date = time.strftime("%d-%b-%Y", dt)
    return date

def get_numbers(str):
    m = re_num.search(str)
    nums = m.group(1)
    return nums
    
    

folder = "D:/laura/modis"
img_type = "EVI"
img_subset = 1
glob = "%s*2000*" % img_type
glob = "%s*" % img_type

bmp_folder = os.path.join (folder, "bmp_" + img_type)
try:
    os.mkdir (bmp_folder)
except:
    pass

mxd = arcpy.mapping.MapDocument("D:/laura/ToBMPreferenceMap3.mxd")
arcpy.env.workspace = folder
rasterList = arcpy.ListRasters (glob)
print (rasterList)
df =  arcpy.mapping.ListDataFrames(mxd, "EVI")[0]

lyr = None
for lyr_i in arcpy.mapping.ListLayers(mxd):
    if lyr_i.name == "EVI":
        lyr = lyr_i
        continue

print (lyr)

i = -1
for raster in rasterList:
    #  only do a subset of images
    i = i + 1
    if i % img_subset != 0:
        continue

    try:
        lyr.replaceDataSource(folder, "RASTER_WORKSPACE", raster)
    except Exception as e:
        raise e

    out_img = os.path.join (bmp_folder, string.replace(raster, "img","bmp"))
    if os.path.isfile(out_img):
        print "Skipping %s, file exists" % out_img
        continue
    
    print "Saving %s as %s" % (raster, out_img)
    try:
        name = get_date_string( get_numbers(raster) )
        print "Name is %s" % name
        df.name = name
        
        arcpy.mapping.ExportToBMP(mxd, out_img)
    except Exception as e:
        raise e

#  A little cleanup.
#  Might stop the problem of being unable to edit 
#   and save the map document.
try:
    del (mxd, df, lyr)
except:
    pass

print "completed"

