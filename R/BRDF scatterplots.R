EVIbrdf=read.table("EVI_BRDF.txt", sep = ",", header = T)
NDVIbrdf=read.table("NDVI_BRDF.txt", sep = ",", header = T)

                
par (mfrow=c(3,4))

plot ("NDVIbrdf$jan02ndvi~NDVIbrdf$NDVI_A2002001", xlim = x, ylim = y, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$jan02ndvi~NDVIbrdf$NDVI_A2002001)
abline (lm, lwd = 2)

plot (NDVIbrdf$apr02ndvi~NDVIbrdf$NDVI_A2002097, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "7 April 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$apr02ndvi~NDVIbrdf$NDVI_A2002097)
abline (lm, lwd = 2)

plot (NDVIbrdf$jul02ndvi~NDVIbrdf$NDVI_A2002193, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "12 July 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$jul02ndvi~NDVIbrdf$NDVI_A2002193)
abline (lm, lwd =2)

plot (NDVIbrdf$oct02ndvi~NDVIbrdf$NDVI_A2002289, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "16 October 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$oct02ndvi~NDVIbrdf$NDVI_A2002289)
abline (lm, lwd = 2)

plot (NDVIbrdf$jan07ndvi~NDVIbrdf$NDVI_A2007001, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "1 January 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$jan07ndvi~NDVIbrdf$NDVI_A2007001)
abline (lm, lwd = 2)

plot (NDVIbrdf$apr07ndvi~NDVIbrdf$NDVI_A2007097, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "7 April 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$apr07ndvi~NDVIbrdf$NDVI_A2007097)
abline (lm, lwd = 2)

plot (NDVIbrdf$jul07ndvi~NDVIbrdf$NDVI_A2007193, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "12 July 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$jul07ndvi~NDVIbrdf$NDVI_A2007193)
abline (lm, lwd =2)

plot (NDVIbrdf$oct07ndvi~NDVIbrdf$NDVI_A2007289, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "16 October 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$oct07ndvi~NDVIbrdf$NDVI_A2007289)
abline (lm, lwd = 2)

plot (NDVIbrdf$jan10ndvi~NDVIbrdf$NDVI_A2010001, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "1 January 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$jan10ndvi~NDVIbrdf$NDVI_A2010001)
abline (lm, lwd = 2)

plot (NDVIbrdf$apr10ndvi~NDVIbrdf$NDVI_A2010097, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "7 April 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$apr10ndvi~NDVIbrdf$NDVI_A2010097)
abline (lm, lwd = 2)

plot (NDVIbrdf$jul10ndvi~NDVIbrdf$NDVI_A2010193, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "12 July 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$jul10ndvi~NDVIbrdf$NDVI_A2010193)
abline (lm, lwd = 2)

plot (NDVIbrdf$oct10ndvi~NDVIbrdf$NDVI_A2010289, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "16 October 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (NDVIbrdf$oct10ndvi~NDVIbrdf$NDVI_A2010289)
abline (lm, lwd = 2)

x = c(min_x,3000)
y = c(min_y,3000)

par (mfrow=c(3,4))

plot (EVIbrdf$jan02evi~EVIbrdf$EVI_A2002001, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "1 January 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$jan02evi~EVIbrdf$EVI_A2002001)
abline (lm, lwd = 2)

plot (EVIbrdf$apr02evi~EVIbrdf$EVI_A2002097, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "7 April 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$apr02evi~EVIbrdf$EVI_A2002097)
abline (lm, lwd = 2)

plot (EVIbrdf$jul02evi~EVIbrdf$EVI_A2002193, xlim = x, ylim = y,  xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "12 July 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$jul02evi~EVIbrdf$EVI_A2002193)
abline (lm, lwd =2)

plot (EVIbrdf$oct02evi~EVIbrdf$EVI_A2002289, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "16 October 2002", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$oct02evi~EVIbrdf$EVI_A2002289)
abline (lm, lwd = 2)

plot (EVIbrdf$jan07evi~EVIbrdf$EVI_A2007001, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "1 January 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$jan07evi~EVIbrdf$EVI_A2007001)
abline (lm, lwd = 2)

plot (EVIbrdf$apr07evi~EVIbrdf$EVI_A2007097, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "7 April 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$apr07evi~EVIbrdf$EVI_A2007097)
abline (lm, lwd = 2)

plot (EVIbrdf$jul07evi~EVIbrdf$EVI_A2007193, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "12 July 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$jul07evi~EVIbrdf$EVI_A2007193)
abline (lm, lwd =2)

plot (EVIbrdf$oct07evi~EVIbrdf$EVI_A2007289, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "16 October 2007", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$oct07evi~EVIbrdf$EVI_A2007289)
abline (lm, lwd = 2)

plot (EVIbrdf$jan10evi~EVIbrdf$EVI_A2010001, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "1 January 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$jan10evi~EVIbrdf$EVI_A2010001)
abline (lm, lwd = 2)

plot (EVIbrdf$apr10evi~EVIbrdf$EVI_A2010097, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "7 April 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$apr10evi~EVIbrdf$EVI_A2010097)
abline (lm, lwd = 2)

plot (EVIbrdf$jul10evi~EVIbrdf$EVI_A2010193, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "12 July 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$jul10evi~EVIbrdf$EVI_A2010193)
abline (lm, lwd = 2)

plot (EVIbrdf$oct10evi~EVIbrdf$EVI_A2010289, xlim = x, ylim = y, xlab = "MODIS EVI value", ylab = "Calculated EVI value", main = "16 October 2010", col = "dark green")
abline (a = 0, b=1, lwd = 2, lty = 5)
lm = lm (EVIbrdf$oct10evi~EVIbrdf$EVI_A2010289)
abline (lm, lwd = 2)


vec = floor (NDVIbrdf$NDVI_A2002097 / 100) * 100 + 50
boxplot (NDVIbrdf$apr02ndvi~vec, xlab = "MODIS NDVI value", ylab = "Calculated NDVI value", main = "7 April 2002", col = "dark green")



plotem_ndvi = function (
                    x, y, 
                    xlab = "Modis NDVI value", 
                    ylab = "Calculated NDVI value", 
                    main = "You forgot to give the main argument",
                    ...
  ) {
  minval = min (x, y)
  maxval = max (x, y)
  lims = c(minval, maxval)
  #print (lims)
  smoothScatter (
    x ~ y, 
    xlim = lims, 
    ylim = lims, 
    xlab = xlab, 
    ylab = ylab, 
    main=main, 
    ...
  )
  abline (a = 0, b=1, lwd = 1.5, lty = 5)
  abline (lm (x ~ y), lwd = 1.5)
  
}

plotem_evi = function (
  x, y, 
  xlab = "Modis EVI value", 
  ylab = "Calculated EVI value", 
  main = "You forgot to give the main argument",
  ...
) {
  minval = min (x, y)
  maxval = max (x, y)
  lims = c(minval, maxval)
  #print (lims)
  smoothScatter (
    x ~ y, 
    xlim = lims, 
    ylim = lims, 
    xlab = xlab, 
    ylab = ylab, 
    main=main, 
    ...
  )
  abline (a = 0, b=1, lwd = 1.5, lty = 5)
  abline (lm (x ~ y), lwd = 1.5)
  
}

#NDVI

plotem_ndvi (NDVIbrdf$jan02ndvi,NDVIbrdf$NDVI_A2002001, main = "1 January 2002")
plotem_ndvi (NDVIbrdf$apr02ndvi,NDVIbrdf$NDVI_A2002097, main = "7 April 2002")
plotem_ndvi (NDVIbrdf$jul02ndvi,NDVIbrdf$NDVI_A2002193, main = "12 July 2002")
plotem_ndvi (NDVIbrdf$oct02ndvi,NDVIbrdf$NDVI_A2002289, main = "16 October 2002")
plotem_ndvi (NDVIbrdf$jan07ndvi,NDVIbrdf$NDVI_A2007001, main = "1 January 2007")
plotem_ndvi (NDVIbrdf$apr07ndvi,NDVIbrdf$NDVI_A2007097, main = "7 April 2007")
plotem_ndvi (NDVIbrdf$jul07ndvi,NDVIbrdf$NDVI_A2007193, main = "12 July 2007")
plotem_ndvi (NDVIbrdf$oct07ndvi,NDVIbrdf$NDVI_A2007289, main = "16 October 2007")
plotem_ndvi (NDVIbrdf$jan10ndvi,NDVIbrdf$NDVI_A2010001, main = "1 January 2010")
plotem_ndvi (NDVIbrdf$apr10ndvi,NDVIbrdf$NDVI_A2010097, main = "7 April 2010")
plotem_ndvi (NDVIbrdf$jul10ndvi,NDVIbrdf$NDVI_A2010193, main = "12 July 2010")
plotem_ndvi (NDVIbrdf$oct10ndvi,NDVIbrdf$NDVI_A2010289, main = "16 October 2010")

#EVI

plotem_evi (EVIbrdf$jan02evi,EVIbrdf$EVI_A2002001, main = "1 January 2002")
plotem_evi (EVIbrdf$apr02evi,EVIbrdf$EVI_A2002097, main = "7 April 2002")
plotem_evi (EVIbrdf$jul02evi,EVIbrdf$EVI_A2002193, main = "12 July 2002")
plotem_evi (EVIbrdf$oct02evi,EVIbrdf$EVI_A2002289, main = "16 October 2002")
plotem_evi (EVIbrdf$jan07evi,EVIbrdf$EVI_A2007001, main = "1 January 2007")
plotem_evi (EVIbrdf$apr07evi,EVIbrdf$EVI_A2007097, main = "7 April 2007")
plotem_evi (EVIbrdf$jul07evi,EVIbrdf$EVI_A2007193, main = "12 July 2007")
plotem_evi (EVIbrdf$oct07evi,EVIbrdf$EVI_A2007289, main = "16 October 2007")
plotem_evi (EVIbrdf$jan10evi,EVIbrdf$EVI_A2010001, main = "1 January 2010")
plotem_evi (EVIbrdf$apr10evi,EVIbrdf$EVI_A2010097, main = "7 April 2010")
plotem_evi (EVIbrdf$jul10evi,EVIbrdf$EVI_A2010193, main = "12 July 2010")
plotem_evi (EVIbrdf$oct10evi,EVIbrdf$EVI_A2010289, main = "16 October 2010")



