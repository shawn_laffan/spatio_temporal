#!perl

use 5.010;
use strict;
use warnings;
use Carp;

use LWP::UserAgent;
#use LWP::Simple;
use DateTime;
use DateTime::Format::Strptime;
use English qw /-no_match_vars/;

our $VERSION = 0.1;

local $| = 1;

my %urls = (
    VI         => 'http://e4ftl01.cr.usgs.gov/MOLT/MOD13Q1.005',
    brdf_model => 'http://e4ftl01.cr.usgs.gov/MOTA/MCD43A1.005',
    brdf_refl  => 'http://e4ftl01.cr.usgs.gov/MOTA/MCD43A4.005',
    VI_aqua    => 'http://e4ftl01.cr.usgs.gov/MOLA/MYD13Q1.005',
    fire       => 'http://e4ftl01.cr.usgs.gov/MOLT/MOD14A2.005',
);

#  need this for the pp build to work
BEGIN {
    if ($ENV{BDV_PP_BUILDING}) {
        use utf8;
        say 'Building pp file';
        say "using $0";
        #use File::BOM qw / :subs /;          #  we need File::BOM.
        #open my $fh, '<:via(File::BOM)', $0  #  just read ourselves
        #  or croak "Cannot open $Bin via File::BOM\n";
        #$fh->close;
    
        #  exercide the unicode regexp matching - needed for the spatial conditions
        use 5.016;
        use feature 'unicode_strings';
        my $string = "sp_self_only () and \N{WHITE SMILING FACE}";
        $string =~ /\bsp_self_only\b/;

        exit();
    }
}



#  get modis tile coords from http://landweb.nascom.nasa.gov/cgi-bin/developer/tilemap.cgi
#  Aus/NZ is 27/10 to 32/13
my $rowcol     = shift @ARGV;
my $start_date = shift @ARGV;
my $end_date   = shift @ARGV;
my $type       = shift @ARGV;

die usage('tile_coords argument not defined')
  if not defined $rowcol;
die usage('tile argument not valid.')
  if not $rowcol =~ 'h\d\dv\d\d';  #  should allow regex arg
die usage('start_date not defined')
  if !defined $start_date;
die usage ('end_date not defined')
  if !defined $end_date;

my $url = $urls{$type} or croak usage ("Invalid image type $type");

my $strp = DateTime::Format::Strptime->new(pattern => '%d/%m/%Y');
my $start_dt = eval {
    $strp->parse_datetime($start_date);
};
croak $EVAL_ERROR if $EVAL_ERROR;
my $end_dt = eval {
    $strp->parse_datetime($end_date);
};
croak $EVAL_ERROR if $EVAL_ERROR;

#  get the julian days to make comparisons easier
my $start_jd = $start_dt->jd;
my $end_jd   = $end_dt->jd;

FOLDER:
foreach my $folder (find_folders (url => $url)) {
    #print $folder . "\n";

    my $folder_url .= "$url/$folder";
    my $file = find_file (url => $folder_url, rowcol => $rowcol);
    my $file_url .= "$folder_url/$file";
    
    next FOLDER if !$file;

    if (-e $file) {
        print "Skipping $file\n";
        next FOLDER;
    }
    
    print "Getting $file\n";
    
    getstore($file_url, $file)
      or die "Could not download file";
    sleep (1 + rand 5);
}
print "Done";

#########################################
##
## Subs
##
##
#########################################

sub find_file {
    my %args   = @_;
    my $url    = $args{url};
    my $rowcol = $args{rowcol};

    my $page = get_page (url => $url);
    my @segments = split ("\n", $page);
    
    my $re_hdf_file = qr /([\d\w.]+?\.hdf)[^.]/;

    foreach my $seg (@segments) {
        next if not $seg =~ $re_hdf_file;
        my $file;
        if ($seg =~ /$re_hdf_file/) {
            $file = $1;
            next if not $file =~ /$rowcol/;
            #print $file . "\n";
            return $file;
        }
    }

    return;
}

#  get the HTML version of the ftp site and process it for folder names
sub find_folders {
    my %args = @_;
    my $url = $args{url};

    my @segments = split ("\n", get_page (url => $url));
    my @folders;
    
    my $re_date = qr /\d{4}\.\d{2}\.\d{2}/;
    
    SEGMENT:
    foreach my $seg (@segments) {
        next SEGMENT if not $seg =~ $re_date;
        my $folder;
        if ($seg =~ /($re_date)/) {
            $folder = $1;
            #print $folder . "\n";
            my $strp = DateTime::Format::Strptime->new(pattern => '%Y.%m.%d');
            my $t = eval {
                $strp->parse_datetime ($seg);
            };
            croak $EVAL_ERROR if $EVAL_ERROR;
            my $jd = $t->jd;
            
            next SEGMENT if $jd < $start_jd || $jd > $end_jd;

            push @folders, $folder;
        }
    }
    
    return wantarray ? @folders : \@folders;
}

sub get_page {
    my %args = @_;
    my $url = $args{url};
    
    my $ua = LWP::UserAgent->new;
    my $req = HTTP::Request->new(GET => $url);
    $req->header(Accept => 'text/html');
    
    my $res = $ua->request($req);
    
    die "Error: " . $res->status_line . "\n"
      if not $res->is_success;

    return $res->decoded_content;
}

sub usage {
    my $comment = shift;
    return <<"END_OF_USAGE"

Usage:

$0 tile_coords start_date end_date image_type

$comment

Tile coords can be gleaned from http://landweb.nascom.nasa.gov/cgi-bin/developer/tilemap.cgi
tile_coords are formatted as a single string, e.g. h29v12
Dates must be in the format dd/mm/YYYY
The type can currently be VI, VI_aqua, brdf_model, brdf_refl or fire.
END_OF_USAGE
  ;
}
