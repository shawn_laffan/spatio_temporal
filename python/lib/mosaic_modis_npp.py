"""Mosaic a set of MODIS tiles"""


# Import system modules
import sys, string, os
import re
import arcpy
import arcpy.management as arcmgt
from random import randint
#from arcpy.sa import *

bands = {
    "GPP": 0,
    "NPP": 1,
    "QC": 2,
}

pixel_type = {
    "GPP": '16_BIT_SIGNED',
    "NPP": '16_BIT_SIGNED',
    "QC":  '8_BIT_UNSIGNED',
}

#  invert bands
layer_names_dict = dict((str(v),k) for k, v in bands.iteritems())

re_date = re.compile("(A\d{7})")

def touch(fname, times = None):
    """
    Create a placeholder file.
    Need to handle geodatabase cases
    (search for .gdb in name and then use an arcpy function to create the file).
    """
    with file(fname, 'a'):
        os.utime(fname, times)
    #os.remove(fname)
    return

def delete_scratch_files (file_list):
    for raster in file_list:
        if arcpy.Exists(raster):
            try:
                arcmgt.Delete(raster)
            except:
                print "Unable to delete temp file %s" % raster
    return

#  Should use an environments object as an optional arg (or a saved env.xml file)
#  This will make for better handling of the env related args.
#  use: arcpy.LoadSettings
#
#  Should also use a dict to handle the glob results, allocating by date
#  This allows a a more generic glob like "*.a2010???.*"
if __name__ == "__main__":
    out_extent   = arcpy.GetParameterAsText (0)
    target_names = arcpy.GetParameterAsText (1)
    target_layer = arcpy.GetParameter (2)
    out_prefix   = arcpy.GetParameterAsText (3)
    workspace    = arcpy.GetParameterAsText (4)

    if len(workspace):
        arcpy.env.workspace = workspace
    if arcpy.env.workspace is None:
        arcpy.env.workspace = os.getcwd()
        workspace = arcpy.env.workspace

    print "Workspace is %s" % arcpy.env.workspace
    
    if target_layer is None or len(target_layer) == 0:
        target_layer = 1  #  default to EVI
    else:
        try:
            x = int(target_layer)
        except:
            target_layer = bands[target_layer]
    
    target_layer_name = layer_names_dict[str(target_layer)]

    print "target layer is %s (#%d)" % (target_layer_name, target_layer)
    
    pixel_type = pixel_type[target_layer_name]
    print 'Pixel type is %s' % pixel_type

    if out_prefix is None or len(out_prefix) == 0 or out_prefix == "#":
        out_prefix = target_layer_name + "_"

    rasters = []
    i = 0
    for glob in string.split(target_names, ";"):
        print "glob: %s" % glob
        raster_list = sorted (arcpy.ListRasters (glob))
        rasters.extend(raster_list)
    
    if not rasters:
        raise Exception ("No rasters satisfy glob")
    else:
        print "Processing %d rasters" % len (rasters)
    
    rasters = sorted (rasters)
    r_dict = {}
    for raster in rasters:
        m = re.search(re_date, raster)
        date = m.group(0)
        try:
            r_dict[date].append(raster)
        except KeyError:
            r_dict[date] = [raster]
        except:
            raise
    for date in r_dict.keys():
        r_dict[date] = sorted (r_dict[date])
    
    arcpy.env.extent     = out_extent
    arcpy.env.snapRaster = rasters[0]
    arcpy.env.overwriteOutput = True
    #arcpy.CheckOutExtension("Spatial")
    env_pyramid = arcpy.env.pyramid
    env_stats   = arcpy.env.rasterStatistics

    arcpy.env.scratchWorkspace = r'D:\temp'  #  dirty hack
    
    print "Extent is %s" % arcpy.env.extent

    for year_day in reversed (sorted (r_dict.keys())):
        try:
            name = "%s%s.img" % (out_prefix, year_day)
            print "Processing %s" % name
            if arcpy.Exists(name):
                print "Output exists, skipping"
                continue
            touch (name)

            arcpy.env.pyramid = 'NONE'
            arcpy.env.rasterStatistics = 'NONE'

            file_list = []
            try:
                for tgt in r_dict[year_day]:
                    scratch = arcpy.CreateScratchName("r_", "_%s" % name, '#', arcpy.env.scratchWorkspace)
                    print "Extracting from %s (%d/%d)" % (tgt, (len(file_list)+1), len(r_dict[year_day]))
                    arcmgt.ExtractSubDataset(tgt, scratch, target_layer)
                    file_list.append(scratch)
            except Exception as e:
                print e
                print "Unable to create scratch file %s for some reason, skipping %s" % (scratch, name)
                delete_scratch_files(file_list)
                os.remove(name)
                continue

            files = string.join(file_list, ";")
            #print "Mosaicing %s into %s" % (files, name)
            try:
                #arcpy.env.pyramid          = env_pyramid
                #arcpy.env.rasterStatistics = env_stats
                try:
                    os.remove(name)
                except Exception as e:
                    print "Cannot delete %s" % name
                    raise e
                print "Mosaic args are:\n%s, %s, %s, %s" % (files, workspace, name, pixel_type)
                arcmgt.MosaicToNewRaster(files, workspace, name, "#", pixel_type, "#", 1)
            except Exception as e:
                #print e
                delete_scratch_files(file_list)  #  clean up and keep going
                try:
                    arcmgt.Delete(name)
                except:
                    pass
                print arcpy.GetMessages()
                continue
                #raise e
            
            delete_scratch_files(file_list)

        except Exception as e:
            print arcpy.GetMessages()
            raise

    print "Processing finished"
