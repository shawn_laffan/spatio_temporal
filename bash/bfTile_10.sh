#!/bin/bash -login
#PBS -N Tile_10
#PBS -W group_list=ed4
#PBS -P ed4
#PBS -q workq
#PBS -l select=1:ncpus=8:mem=4G,walltime=10:00:00

ulimit -s unlimited
cd $PBS_O_WORKDIR
echo `pwd`
echo $PBS_O_WORKDIR

source /usr/share/modules/init/bash
export BFAST_INPUT_DATA=/projects/ed4/input_data
export BFAST_USE_HPC=8

Rscript /projects/ed4/repos/spatio_temporal/R/process_EVI_tile.R EVI_1km_2000_2012_10.csv /projects/ed4/input_data

