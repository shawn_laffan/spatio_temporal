#setwd()


create16dayts <- function(data,dates) {
  z <- zoo(data,dates)
  yr <- as.numeric(format(time(z), "%Y"))
  jul <- as.numeric(format(time(z), "%j"))
  delta <- min(unlist(tapply(jul, yr, diff))) # 16
  zz <- aggregate(z, yr + (jul - 1) / delta / 23)
  (tso <- as.ts(zz))
  return(tso)  
}


NDVIfield2000_2002 = read.csv("NDVIfield_2000_2002.txt", row.names = 2)
NDVIfield2003_2005 = read.csv("NDVIfield_2003_2005.txt", row.names = 2)
NDVIfield2006_2008 = read.csv("NDVIfield_2006_2008.txt", row.names = 2)
NDVIfield2009_2012 = read.csv("NDVIfield_2009_2012.txt", row.names = 2)

startcol = 4

zz0002 = length(colnames(NDVIfield2000_2002))
zz0305 = length(colnames(NDVIfield2003_2005))
zz0608 = length(colnames(NDVIfield2006_2008))
zz0912 = length(colnames(NDVIfield2009_2012))

data = t(cbind (NDVIfield2000_2002[,startcol:zz0002], NDVIfield2003_2005[,startcol:zz0305], NDVIfield2006_2008[,startcol:zz0608], NDVIfield2009_2012[,startcol:zz0912]))

dates = as.Date(rownames(data), "NDVI_A%Y%j")


z = list()
for (i in 1:length(colnames(data))) {
  z[[i]] = zoo(data[,i], order.by=dates)
}

t16 = list()
for (i in 1:length(colnames(data))) {
  t16[[i]] = create16dayts (data[,i], dates)
}

#  panel plot
par (mfrow=c(5,6))
for (i in 1:length(t16)) {plot (t16[[i]], ylab = paste ("Sample ", i))}

# Call Functions



bf  = list()
pscores = list()
breakdates_all= list()
breakdates = list()
magnitudes = list()
dw_test = list()
h_param = 1/7

for (i in 1:length(colnames(data))) {
  print (paste ("sample ", i, sep=""))
  bf[[i]]  = bfast (t16[[i]], h = h_param, season = "dummy", max.iter = 10, breaks = NULL)
  pscores[[i]] = bf_get_p (bf[[i]])
  breakdates_all[[i]] = bf_get_breakdates (bf[[i]])
  breakdates [[i]] = get_breakdate_from_bf_list (bf [[i]])
  magnitudes [[i]] = bf[[i]]$Magnitude
  dw_test [[i]] = durbin_watson (bf [[i]])
}

capture.output(pscores, file = "pscores.txt", append = TRUE)
capture.output(breakdates, file = "breakdates2.txt", append = TRUE)
capture.output (magnitudes, file = "magnitudes.txt", append = TRUE)
capture.output (dw_test, file = "autocorrelation.txt", append = TRUE)

for (i in 1:70) {bmp(paste("ndvifield_",i,".bmp",sep="")); plot (bf[[i]], ANOVA=T, main = paste("Field Site", i, "NDVI")); dev.off()}
for (i in 1:70) {png(paste("ndvifield__",i,".png",sep="")); plot (bf[[i]], ANOVA=T, main = paste("Field Site", i, "NDVI")); dev.off()}
for (i in 1:70) {png(paste("ndvifield__noise",i,".png",sep="")); plot (bf[[i]],type ="noise",ylab = "Remainder Component", main = paste("Field Site", i, "NDVI")); dev.off()}

