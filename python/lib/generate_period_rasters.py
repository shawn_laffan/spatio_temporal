import arcpy
import numpy as np
import string
import os

from arcpy import env
#from arcpy.sa import *

class NoRasters(Exception):
    pass

env.overwriteOutput = True
env.workspace = r"D:\laura\rasters_no_one_third\all_rasters"
if not os.path.exists(env.workspace):
    env.workspace = os.getcwd()
out_pfx = 'pd'

#  all rasters must be of the same extent
rasterList = arcpy.ListRasters ('Trend_Breakdate_*.asc')

if not rasterList:
    raise NoRasters ("No rasters match query")


np_rasters = []
for raster_name in rasterList:
    #desc = arcpy.Describe(raster_name)
    this_ras = arcpy.RasterToNumPyArray(raster_name)  #  read this raster
    np_rasters.append(this_ras)
    #out_ras = arcpy.NumPyArrayToRaster (this_ras)  #  for debug

n_timesteps = 296
name_fmt = "%s_%0" + str(len(str(n_timesteps))) + "i.img"  #  allow for differing sequence lengths

max_timestep = np_rasters[-1].max()

#  numpy array storing the value of which period we are in
#  np.ones_like does not work with conversion to raster, not sure why
period_ras = np.zeros_like(np_rasters[0])


desc = arcpy.Describe(rasterList[-1])
nodata = desc.noDataValue
cellsize = desc.meanCellWidth
ll_pt = arcpy.Point (desc.extent.XMin, desc.extent.YMin)

#  ugly hack as Arc does not like np.ones_like() or np.zeros_like()+1
period_ras = np.where (period_ras == 0, 1, period_ras)

for timestep in range (1, n_timesteps+1):
    
    for this_ras in np_rasters:    
        period_ras = np.where (this_ras == timestep, period_ras + 1, period_ras)

    print period_ras.min(), period_ras.max()

    #out_ras = arcpy.NumPyArrayToRaster (period_ras)
    out_ras = arcpy.NumPyArrayToRaster (period_ras, ll_pt, cellsize)

    out_name = name_fmt % (out_pfx, timestep)
    print out_name
    out_ras.save(out_name)

