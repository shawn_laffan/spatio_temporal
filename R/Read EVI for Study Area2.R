# setwd()
install.packages("foreach")
setwd("D:/laura/modis/Sample Tables")

library (bfast)
library (lmtest)

source('D:/laura/spatio_temporal/R/BFAST Functions.R', echo=TRUE)

EVI_1km2000_2002 = list()
EVI_1km2003_2005 = list()
EVI_1km2006_2008 = list()
EVI_1km2009_2012 = list()

start_file = 5
end_file = 5

for (i in start_file:end_file) {
  fname1 = paste ("EVI_1kmArea", i, "_2000_2002.txt", sep = "")
  fname2 = paste ("EVI_1kmArea", i, "_2003_2005.txt", sep = "")
  fname3 = paste ("EVI_1kmArea", i, "_2006_2008.txt", sep = "")
  fname4 = paste ("EVI_1kmArea", i, "_2009_2012.txt", sep = "")
  
  EVI_1km2000_2002[[i]] = read.csv (fname1, row.names = 2)
  EVI_1km2003_2005[[i]] = read.csv (fname2, row.names = 2)
  EVI_1km2006_2008[[i]] = read.csv (fname3, row.names = 2)
  EVI_1km2009_2012[[i]] = read.csv (fname4, row.names = 2)
  
}


startcol = 4


for (i in 5:length(EVI_1km2000_2002)) {

  df1 = EVI_1km2000_2002[[i]]
  df2 = EVI_1km2003_2005[[i]]
  df3 = EVI_1km2006_2008[[i]]
  df4 = EVI_1km2009_2012[[i]]
  len1 = length(df1)
  len2 = length(df2)
  len3 = length(df3)
  len4 = length(df4)

  data = t(cbind (df1[,startcol:len1], df2[,startcol:len2], df3[,startcol:len3], df4[,startcol:len4]))
  
  coords = df1[,2:3]

  dates = as.Date(rownames(data), "EVI_A%Y%j")
  
  run_bf_for_df(data, coords = coords, file_pfx = paste ("bfast_res_", i, sep = ""))

}
