#setwd()


create16dayts <- function(data,dates) {
	z <- zoo(data,dates)
	yr <- as.numeric(format(time(z), "%Y"))
	jul <- as.numeric(format(time(z), "%j"))
	delta <- min(unlist(tapply(jul, yr, diff))) # 16
	zz <- aggregate(z, yr + (jul - 1) / delta / 23)
	(tso <- as.ts(zz))
	return(tso)  
}


EVI2000_2003 = read.csv("SamplePts_2000_2003.txt", row.names = 2)
EVI2004_2007 = read.csv("SamplePts_2004_2007.txt", row.names = 2)
EVI2008_2012 = read.csv("SamplePts_2008_2012.txt", row.names = 2)

startcol = 4

zz0003 = length(colnames(EVI2000_2003))
zz0407 = length(colnames(EVI2004_2007))
zz0812 = length(colnames(EVI2008_2012))

data = t(cbind (EVI2000_2003[,startcol:zz0003], EVI2004_2007[,startcol:zz0407], EVI2008_2012[,startcol:zz0812]))

dates = as.Date(rownames(data), "EVI_A%Y%j")


z2 = zoo(data[,2], order.by = dates)
plot (z2, lty = 1, main="ggghgghg", col="red")
points (z2, pch = 2)

z = list()
for (i in 1:length(colnames(data))) {
    z[[i]] = zoo(data[,i], order.by=dates)
}

t16 = list()
for (i in 1:length(colnames(data))) {
    t16[[i]] = create16dayts (data[,i], dates)
}

#  panel plot
par (mfrow=c(5,5))
for (i in 1:length(t16)) {plot (t16[[i]], ylab = paste ("Sample ", i))}
