#setwd()


create16dayts <- function(data,dates) {
  z <- zoo(data,dates)
  yr <- as.numeric(format(time(z), "%Y"))
  jul <- as.numeric(format(time(z), "%j"))
  delta <- min(unlist(tapply(jul, yr, diff))) # 16
  zz <- aggregate(z, yr + (jul - 1) / delta / 23)
  (tso <- as.ts(zz))
  return(tso)  
}

reliability2000_2002 = read.csv("reliability_2000_2002.txt", row.names = 2)
reliability2003_2005 = read.csv("reliability_2003_2005.txt", row.names = 2)
reliability2006_2008 = read.csv("reliability_2006_2008.txt", row.names = 2)
reliability2009_2012 = read.csv("reliability_2009_2012.txt", row.names = 2)

startcol = 4

zz0002 = length(colnames(reliability2000_2002))
zz0305 = length(colnames(reliability2003_2005))
zz0608 = length(colnames(reliability2006_2008))
zz0912 = length(colnames(reliability2009_2012))

data = t(cbind (reliability2000_2002[,startcol:zz0002], reliability2003_2005[,startcol:zz0305], reliability2006_2008[,startcol:zz0608], reliability2009_2012[,startcol:zz0912]))

dates = as.Date(rownames(data), "reliability_A%Y%j")


z = list()
for (i in 1:length(colnames(data))) {
  z[[i]] = zoo(data[,i], order.by=dates)
}

t16 = list()
for (i in 1:length(colnames(data))) {
  t16[[i]] = create16dayts (data[,i], dates)
}

#  panel plot
par (mfrow=c(5,6))
for (i in 1:length(t16)) {plot (t16[[i]], ylab = paste ("Sample ", i))}
