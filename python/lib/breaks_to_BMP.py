import arcpy
import string
import os
import time
import datetime
import re

re_num = re.compile("(\d+)")

date_fmt   = "%d-%b-%Y"
start_date = datetime.datetime.strptime('18-Feb-2000', date_fmt)
time_delta = datetime.timedelta (16)


def get_date_string_dict (max_num = 300):
    date_string_dict = {}
    
    date_string_dict[1] = datetime.datetime.strftime(start_date, date_fmt)
    last_date = start_date

    for i in range (2, (max_num+1)):
        this_date = last_date + time_delta

        #  make sure we get Jan 1 for first image in each year
        if this_date.month == 1 and this_date.day < 17:
            yr = this_date.year
            this_date = datetime.datetime (yr, 1, 1)
    
        date_string_dict[i] = datetime.datetime.strftime(this_date, date_fmt)
        last_date = this_date

    return date_string_dict

def get_numbers(str):
    m = re_num.search(str)
    nums = m.group(1)
    return int (nums)

folder = "D:/laura/all_rasters"
arcpy.env.workspace = folder
glob = "pd_*.img"

date_string_dict = get_date_string_dict()

bmp_folder = os.path.join (folder, "bmp_breaks")
try:
    os.mkdir (bmp_folder)
except:
    pass

mxd_file = "D:/laura/Maps/Break Timing to BMP.mxd"
#mxd_file = r"D:\shawn\svn\spatio_temporal\python\Break Timing to BMP.mxd"
mxd = arcpy.mapping.MapDocument(mxd_file)

arcpy.env.workspace = folder
rasterList = arcpy.ListRasters (glob)
print (rasterList)
df =  arcpy.mapping.ListDataFrames(mxd, "Breaks")[0]

lyr = None
for lyr_i in arcpy.mapping.ListLayers(mxd):
    if lyr_i.name == "No. of Breaks":  ###
        lyr = lyr_i
        continue

text_elt_prop_name = None
for elt in arcpy.mapping.ListLayoutElements (mxd, "TEXT_ELEMENT"):
    if elt.name == "map_title":
        text_elt_prop_name = elt
        break


print (lyr)

last_date = None

i = -1
for raster in rasterList:
    #  only do a subset of images
    i = i + 1
    #if i % img_subset != 0:
        #continue

    try:
        lyr.replaceDataSource(folder, "RASTER_WORKSPACE", raster)
    except Exception as e:
        raise e

    out_img = os.path.join (bmp_folder, string.replace(raster, "img", "bmp"))
    if os.path.isfile(out_img):
        print "Skipping %s, file exists" % out_img
        continue
    
    print "Saving %s as %s" % (raster, out_img)
    try:
        date = date_string_dict[get_numbers(raster)]
        print "Date for %s is %s" % (raster, date)
        text_elt_prop_name.text = date
        last_date = date

        arcpy.mapping.ExportToBMP(mxd, out_img)
    except Exception as e:
        raise e

#  A little cleanup.
#  Might stop the problem of being unable to edit 
#   and save the map document.
try:
    del (mxd, df, lyr)
except:
    pass

print "completed"

