"""Calculate focalmean surfaces for a set of MODIS tiles"""


# Import system modules
import sys, string, os
#import process_srtm
import re
import arcpy
import arcpy.management as arcmgt
from arcpy.sa import *


#bounds_file = r"D:\shawn\data3\fmd_kimberley\sample_area_modis_snapped.shp"
workspace = r'D:/shawn/data3/fmd_kimberley/modis'
workspace = r'D:/shawn/data3/hendra/modis'

radius = 10000
out_pfx = "fm" + str (radius)
type = 'EVI'
arcpy.env.workspace  = workspace
rasters = arcpy.ListRasters (type + "*")
#rasters = arcpy.ListRasters ("*.hdf")  #  need to handle the original hdf files

analysis_extent = r'D:/shawn/data3/hendra/hendra_locations_2011_buffer.shp'

arcpy.env.overwriteOutput = True
arcpy.env.extent = analysis_extent
arcpy.CheckOutExtension("Spatial")

nbrhood = NbrCircle(radius, "MAP")

for raster in rasters:
    try:
        
        name = out_pfx + '_' + raster
        print "Processing %s" % name
        if arcpy.Exists(name):
            print "Output exists, skipping"
            continue
        
        result = FocalStatistics(raster, nbrhood, "MEAN", "")
        result.save(name)

    except:
        print arcpy.GetMessages()
        print "Failure"
    
print "Finished"
