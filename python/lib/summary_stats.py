#!/usr/bin/env python

"""
Generate summary statistics rasters for a set of input rasters.
"""

# Import system modules
import sys, string, os
#import re
import arcpy
import arcpy.management as arcmgt
from arcpy.sa import CellStatistics

if __name__ == "__main__":
    target_names = arcpy.GetParameterAsText (0)
    out_prefix   = arcpy.GetParameterAsText (1)
    extent       = arcpy.GetParameterAsText (2)


    if arcpy.env.workspace is None:
        arcpy.env.workspace = os.getcwd()
        workspace = arcpy.env.workspace

    if len(target_names) == 0:
        raise "No targets specified"
    
    arcpy.CheckOutExtension("Spatial")
    
    rasters = []
    for glob in string.split(target_names, ";"):
        print "glob: %s" % glob
        raster_list = sorted (arcpy.ListRasters (glob))
        rasters.extend(raster_list)
    rasters_as_string = string.join (rasters, ";")
    
    if extent != '#' and len (extent) > 0:
        arcpy.env.extent = extent
    print "Extent is %s" % arcpy.env.extent

    for stat_type in ('MEAN', 'STD'):
        try:
            name = out_prefix + '_' + stat_type
    
            print "Processing %s" % stat_type
            if arcpy.Exists(name):
                print "Output exists, skipping"
            
            result = CellStatistics (rasters, stat_type)
            result.save(name)

        except Exception as e:
            print arcpy.GetMessages()
            raise