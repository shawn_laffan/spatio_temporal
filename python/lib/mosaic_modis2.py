"""Mosaic a set of MODIS tiles for the Kimberley project"""


# Import system modules
import sys, string, os
import re
import arcpy
import arcpy.management as arcmgt
from random import randint
#from arcpy.sa import *

bands = {
    "NDVI": 0,
    "EVI": 1,
    "VI_Quality": 2,
    "red": 3,
    "NIR": 4,
    "blue": 5,
    "MIR": 6,
    "view_zenith": 7,
    "sun_zenith": 8,
    "rel_azimuth": 9,
    "day_of_yr": 10,
    "reliability": 11,
}

#  invert bands
layer_names_dict = dict((str(v),k) for k, v in bands.iteritems())

re_date = re.compile("(A\d{7})")

def touch(fname, times = None):
    """
    Create a placeholder file.
    Need to handle geodatabase cases
    (search for .gdb in name and then use an arcpy function to create the file).
    """
    with file(fname, 'a'):
        os.utime(fname, times)
    #os.remove(fname)
    return

#  Should use an environments object as an optional arg (or a saved env.xml file)
#  This will make for better handling of the env related args.
#
#  Should also use a dict to handle the glob results, allocating by date
#  This allows a a more generic glob like "*.a2010???.*"
if __name__ == "__main__":
    out_extent   = arcpy.GetParameterAsText (0)
    target_names = arcpy.GetParameterAsText (1)
    target_layer = arcpy.GetParameterAsText (2)
    out_prefix   = arcpy.GetParameterAsText (3)
    workspace    = arcpy.GetParameterAsText (4)

    if len(workspace):
        arcpy.env.workspace = workspace
    if arcpy.env.workspace is None:
        arcpy.env.workspace = os.getcwd()
        workspace = arcpy.env.workspace

    print "Workspace is %s" % arcpy.env.workspace
    
    if target_layer is None or len(target_layer) == 0:
        target_layer = 1  #  default to EVI
    
    if out_prefix is None or len(out_prefix) == 0 or out_prefix == "#":
        out_prefix = layer_names_dict[str(target_layer)] + "_"

    rasters = []
    i = 0
    for glob in string.split(target_names, ";"):
        print glob
        raster_list = sorted (arcpy.ListRasters (glob))
        rasters.append(raster_list)
    
    arcpy.env.extent     = out_extent
    arcpy.env.snapRaster = rasters[0][0]
    arcpy.env.overwriteOutput = True
    #arcpy.CheckOutExtension("Spatial")
    
    print "Extent is %s" % arcpy.env.extent

    for i in range (len(rasters[0])):
        try:
            m = re.search (re_date, rasters[0][i])
            year_day = m.group(0)
            name = "%s%s.img" % (out_prefix, year_day)
            print "Processing %s" % name
            if arcpy.Exists(name):
                print "Output exists, skipping"
                continue
            touch (name)

            file_list = []
            #rand_id = randint(0, 100000)
            for j in range (len(rasters)):
                scratch = arcpy.CreateScratchName("r_", "_%s" % name)
                tgt = rasters[j][i]
                print "Extracting from %s" % tgt
                arcmgt.ExtractSubDataset(tgt, scratch, target_layer)
                file_list.append(scratch)

            files = string.join(file_list, ";")
            print "Mosaicing %s into %s" % (files, name)
            #print "%s %s %s %s %s %s %s" % (files, workspace, name, "#", "16_BIT_SIGNED", "#", 1)
            try:
                #arcmgt.Delete(name)
                os.remove(name)
                arcmgt.MosaicToNewRaster(files, workspace, name, "#", "16_BIT_SIGNED", "#", 1)
            except Exception as e:
                raise e

            for raster in file_list:
                if arcpy.Exists(raster):
                    try:
                        arcmgt.Delete(raster)
                    except:
                        print "Unable to delete temp file %s" % raster

        except:
            print arcpy.GetMessages()
            print "Failure"
            raise

    print "Processing finished"
