#setwd()


create16dayts <- function(data,dates) {
  z <- zoo(data,dates)
  yr <- as.numeric(format(time(z), "%Y"))
  jul <- as.numeric(format(time(z), "%j"))
  delta <- min(unlist(tapply(jul, yr, diff))) # 16
  zz <- aggregate(z, yr + (jul - 1) / delta / 23)
  (tso <- as.ts(zz))
  return(tso)  
}

EVI2000_2002 = read.csv("EVI_2000_2002.txt", row.names = 2)
EVI2003_2005 = read.csv("EVI_2003_2005.txt", row.names = 2)
EVI2006_2008 = read.csv("EVI_2006_2008.txt", row.names = 2)
EVI2009_2012 = read.csv("EVI_2009_2012.txt", row.names = 2)


startcol = 4

zz0002 = length(colnames(EVI2000_2002))
zz0305 = length(colnames(EVI2003_2005))
zz0608 = length(colnames(EVI2006_2008))
zz0912 = length(colnames(EVI2009_2012))

data = t(cbind (EVI2000_2002[,startcol:zz0002], EVI2003_2005[,startcol:zz0305], EVI2006_2008[,startcol:zz0608],EVI2009_2012[,startcol:zz0912]))

dates = as.Date(rownames(data), "EVI_A%Y%j")


z = list()
for (i in 1:length(colnames(data))) {
  z[[i]] = zoo(data[,i], order.by=dates)
}

t16 = list()
for (i in 1:length(colnames(data))) {
  t16[[i]] = create16dayts (data[,i], dates)
}

EVIaqua2002_2005 = read.csv("EVIaqua_2002_2005.txt", row.names = 2)
EVIaqua2006_2009 = read.csv("EVIaqua_2006_2009.txt", row.names = 2)
EVIaqua2010_2012 = read.csv("EVIaqua_2010_2012.txt", row.names = 2)

startcol = 4

zz0205 = length(colnames(EVIaqua2002_2005))
zz0609 = length(colnames(EVIaqua2006_2009))
zz1012 = length(colnames(EVIaqua2010_2012))

aquadata = t(cbind (EVIaqua2002_2005[,startcol:zz0205], EVIaqua2006_2009[,startcol:zz0609], EVIaqua2010_2012[,startcol:zz1012]))

aquadates = as.Date(rownames(aquadata), "EVI_A%Y%j")


zaqua = list()
for (i in 1:length(colnames(aquadata))) {
  zaqua[[i]] = zoo(aquadata[,i], order.by=aquadates)
}

t16_aqua = list()
for (i in 1:length(colnames(aquadata))) {
  t16_aqua[[i]] = create16dayts (aquadata[,i],aquadates)
}

