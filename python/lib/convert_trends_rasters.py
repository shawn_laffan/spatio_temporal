import arcpy
import numpy as np
import string
import os

from arcpy import env
#from arcpy.sa import *

class NoRasters(Exception):
    pass

env.overwriteOutput = True
env.workspace = r"D:\laura\trend_rasters"
if not os.path.exists(env.workspace):
    env.workspace = os.getcwd()
out_pfx = 'pd'

#  all rasters must be of the same extent
rasterList = arcpy.ListRasters ('d*.asc')

if not rasterList:
    raise NoRasters ("No rasters match query")

#rasterList = rasterList[0:10]

#  numpy array storing the value of which period we are in
period_ras = np.zeros_like(arcpy.RasterToNumPyArray(rasterList[0]))
desc = arcpy.Describe(rasterList[0])
nodata = desc.noDataValue
#  numpy array storing the value of the previous raster processed.  Initialise with nodata
prev_ras   = np.ones_like (period_ras) * nodata
precision_correction = 100000

for raster_name in rasterList:
    desc = arcpy.Describe(raster_name)
    nodata = desc.noDataValue * precision_correction
    this_ras = arcpy.RasterToNumPyArray(raster_name)  #  read this raster
    this_ras = np.ceil (np.multiply (this_ras, precision_correction))

    period_ras = np.where ((this_ras != nodata) & (this_ras != prev_ras), period_ras + 1, period_ras)
    print period_ras.min(), period_ras.max()

    ll_pt = arcpy.Point (desc.extent.XMin, desc.extent.YMin)
    out_ras = arcpy.NumPyArrayToRaster (period_ras, ll_pt, desc.meanCellWidth)

    raster_name = string.replace (raster_name, 'asc', 'img')
    out_name = "%s_%s" % (out_pfx, raster_name)
    print out_pfx, raster_name, out_name
    out_ras.save(out_name)

    prev_ras = this_ras
    
