#!/usr/bin/perl -w
use strict;
use warnings;
use 5.010;

my @files = glob '*.sh';

foreach my $file (@files) {
    say $file;
    exec $file;
}

