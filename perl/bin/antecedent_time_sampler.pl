#! /usr/bin/perl
#!perl

use strict;
use warnings;
use Carp;

use English qw { -no_match_vars };

local $| = 1;

use DateTime;
use DateTime::Format::Strptime;
#use Time::Piece;
#use Date::Parse;
use Statistics::Descriptive;
use Data::Printer;

use Text::CSV_XS;
my $csv = Text::CSV_XS->new;


my $in_file  = shift @ARGV;
my $out_file = shift @ARGV;
my $stat     = shift @ARGV || 'mean';

my $id_fld   = 'OBJECTID';
my $date_fld = 'Date_';

open (my $fh,  '<', $in_file)  or croak "Cannot open $in_file for reading\n";
open (my $ofh, '>', $out_file) or croak "Cannot open $out_file for writing\n";


my @col_names = $csv->column_names ($csv->getline ($fh));
my $data = $csv->getline_hr_all ($fh);

my $date_smp_cols = {};
foreach my $key (@col_names) {
    next if not $key =~ /(\d\d\d+)$/;
    next if $key =~ /^OBJECTID/;
    $date_smp_cols->{$key} = $1;
}

my @sorted_date_smp_cols = sort {$date_smp_cols->{$a} <=> $date_smp_cols->{$b}} keys %$date_smp_cols;

my $unit = 16;
my $spacings = [1, 2, 4, 8, 12];
$spacings    = [1, 2, 4, 8];
my $targets = [];
foreach my $spacing (@$spacings) {
    push @$targets, ($unit * $spacing);
}

print "Targets are: " . join q{ }, @$targets, "\n";
#print "xxx\n\n";

$csv->print ($ofh, [$id_fld, 'year_day', @$targets]);
print $ofh "\n";

my $rec_count;
foreach my $row (@$data) {
    $rec_count ++;
    my $id   = $row->{$id_fld};

    my $strp   = DateTime::Format::Strptime->new(pattern => '%d/%m/%Y');
    my $date   = $row->{$date_fld};
    croak "Date is undefined" if !defined $date;
    my $dt     = $strp->parse_datetime ($date);
    my $yr_day = $dt->year . $dt->day_of_year;
    
    my $stats = get_data ($dt, $date_smp_cols, \@sorted_date_smp_cols, $targets, $row);
    #p($stats);
    my @line = ($id, $yr_day);
    foreach my $key (sort {$a<=>$b} keys %$stats) {
        push @line, $stats->{$key}->$stat;
    }
    $csv->print($ofh, \@line);
    print $ofh "\n";
    #print $csv->string, "\n";
}

print "Processed $rec_count records\n";


sub get_data {
    my ($smp_date, $date_smp_cols, $sorted_cols, $targets, $record) = @_;

    my %stats;
    foreach my $target (@$targets) {
        $stats{$target} = Statistics::Descriptive::Full->new;
    }

    my @data;
    foreach my $key (@$sorted_cols) {
        my $strp = DateTime::Format::Strptime->new(pattern => '%Y%j');
        my $img_date = $strp->parse_datetime ($date_smp_cols->{$key});

        #  difference of julian days - allows for multiyear spans
        my $diff = $smp_date->jd - $img_date->jd;
        #print $diff;

        last if $diff <= -$unit;         #  sample date is more recent than the record date, with no overlap
        next if $diff >= $targets->[-1]; #  sample date is too old for any of the targets

        my $wt = $unit;
        if ($diff < 0) {  #  overlap with latest sample
            $wt = $unit + $diff;
        }
        elsif ($diff >= ($targets->[-1] - $unit)) {  #  overlap with oldest sample
            $wt = $unit - $diff % $unit;
        }
        my $record_val = $record->{$key};
        push @data, ($record_val) x $wt;
        printf "%4d%4d%4d%4d\n", $smp_date->day_of_year, $img_date->day_of_year, $diff, $wt;
    }

    #print "\n";
    print scalar @data . " $targets->[-1]\n\n";
    #print join " ", @data, "\n\n";
    my $rec_count = scalar @data;
    croak "Incorrect number of records for $smp_date.  $rec_count != $targets->[-1]. Check you have sufficient image samples"
      if scalar @data != $targets->[-1];



    foreach my $target (@$targets) {
        $stats{$target}->add_data (@data[-$target..-1]);
        print $target . " " . $stats{$target}->count . " ";
    }
    print "\n";    
    
    return wantarray ? %stats : \%stats;
}

