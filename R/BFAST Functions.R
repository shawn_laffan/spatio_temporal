
create16dayts <- function(data,dates) {
  z <- zoo(data,dates)
  yr <- as.numeric(format(time(z), "%Y"))
  jul <- as.numeric(format(time(z), "%j"))
  delta <- min(unlist(tapply(jul, yr, diff))) # 16
  zz <- aggregate(z, yr + (jul - 1) / delta / 23)
  (tso <- as.ts(zz))
  return(tso)  
}

bf_get_p = function (bf) {
  convergence_iter = length (bf$output)
  out = bf$output[[convergence_iter]]
  ft <- cbind(seasonal = out$St, trend = out$Tt, remainder = out$Nt)
  x <- list(time.series = ft)
  sers <- x$time.series
  ncomp <- ncol(sers)
  data <- drop(sers %*% rep(1, ncomp))
  X <- cbind(data, sers)
  
  colnames(X) <- c("Yt","St","Tt", "et")
  
  ## ANOVA
  fit = bf
  if (is.null(fit) == F) {
    niter <- length(fit$output) # nr of iterations
    out <- fit$output[[niter]]  # output of results of the final fitted seasonal and trend models and nr of breakpoints in both.
    out_ANOVA <- array()
    out_breakdates <- array()
    ## line below is updated - JV - was causing problems
    if (out$Vt.bp[1] > 0) {breaks <- length(out$Vt.bp) } else {breaks <- 0}  # number of breaks
    if (breaks > 0) {
      breakdates <- out$Vt.bp # breakdates
      coefs <- coef(out$bp.Vt) # output coefficients per segment  
      sl <- coefs[,2] # slopes
    }
    
    TS_anova <- fit$Yt - out$St   # time series Yt - St for ANOVA test
    dataframe <- data.frame(TIME=c(1:length(fit$Yt)),DATA=TS_anova)
    
    # determine segment startpoint and endpoint, calculate ANOVA
    for (m in 1:(breaks+1)) {
      startpoint <- if(m==1)  1 else breakdates[[m-1]]
      endpoint <- if(m==(breaks+1)) length(fit$Yt) else breakdates[m]-1
      df2 <- dataframe[startpoint:endpoint,]  # subset of dataframe (section)
      model <- lm(DATA~TIME, data=df2)        # linear model
      modelAnova <- anova(model)              # ANOVA
      out_ANOVA[m] <- modelAnova$Pr[1]        # save p-value  
      if(breaks==0) {sl <- model$coefficients[2]}  ## JV updated -- this was causing problems !# slope Tt if breaks == 0
    }
    
  }
  
  return (out_ANOVA)
}

bf_get_breakdates = function (bf) {
  convergence_iter = length (bf$output)
  print (paste ("convergence at: ", convergence_iter))
  out = bf$output[[convergence_iter]]
  ft <- cbind(seasonal = out$St, trend = out$Tt, remainder = out$Nt)
  x <- list(time.series = ft)
  sers <- x$time.series
  ncomp <- ncol(sers)
  data <- drop(sers %*% rep(1, ncomp))
  X <- cbind(data, sers)
  
  colnames(X) <- c("Yt","St","Tt", "et")
  
  fit = bf
  if (is.null(fit) == F) {
    niter <- length(fit$output) # nr of iterations
    out <- fit$output[[niter]]  # output of results of the final fitted seasonal and trend models and nr of breakpoints in both.
    
    if (out$Vt.bp[1] > 0) {breaks <- length(out$Vt.bp) } else {breaks <- 0}  # number of breaks
    if (breaks > 0) {
      breakdates <- out$Vt.bp # breakdates
    }
  }
  
  return (breakdates)
}

get_trend_breakdates = function (bf) {
  iter = length (bf$output)
  return (bf$output[[iter]]$Vt.bp)
}

get_seasonal_breakdates = function (bf) {
  iter = length (bf$output)
  return (bf$output[[iter]]$Wt.bp)
}

bf_get_ci = function (bf) {
  iter = length (bf$output)
  return (bf$output[[iter]]$ci.Vt)
}

durbin_watson = function (bf) {
  iter = length (bf$output)
  print (paste ("DW test iter is", iter))
  noise = bf$output[[iter]]$Nt
  resid.lm=lm(noise ~ time(noise))
  dwtest = dwtest(resid.lm, alternative = "two.sided")
  return (dwtest)
  
}

#  input is a ts() object, fed through the t16 function
run_bfast_with_trimmings = function (t16, alpha = 0.05) {
  library ("lmtest")

  results = list(bf = NA, pscores = NA, trend_breakdates = NA, seasonal_breakdates = NA, magnitudes = NA, time = NA, dw_test = NA, Tt = NA, h_param = NA)
  #results = list()
  #names(results = c("bf", "pscores", "trend_breakdates","seasonal_breakdates", dw_test", "Tt", "h_param"))

  h_param_list = c(1/3, 1/4, 1/5, 1/6, 1/7)
  #h_param_list = c(1/3, 1/5)  #  FOR TESTING
  prev_sig_duration = -1  #  initialise with -1
  
  max_date = length(t16)
  
  for (h_param in h_param_list) {
    print (paste ("h_param is: ", h_param))
    bf = bfast (t16, h = h_param, season = "dummy", max.iter = 10, breaks = NULL, hpc = "foreach")
    pscores              = bf_get_p (bf)
    #breakdates_all      = bf_get_breakdates (bf)  #  NEED TO JUST GET FROM TREND AND SEASONAL
    trend_breakdates    = get_trend_breakdates (bf)
    seasonal_breakdates = get_seasonal_breakdates (bf)
    dw_test        = durbin_watson (bf)
    iter           = length (bf$output)

    #loop over pscores and breakdates, adding breakdate duration if duration of sig p is greater than the stored one
    trend_durations = as.vector(c(trend_breakdates,max_date)) - as.vector(c(0,trend_breakdates))
    sig_periods     = trend_durations[pscores < alpha]
    sig_duration    = sum(sig_periods)

    print (paste ("Trend durations: ", trend_durations))
    print (paste ("sig periods: ", sig_periods))
    print (paste ("sig duration: ", sig_duration))
    
    if (sig_duration > prev_sig_duration) {
    
      results[["bf"]]         = bf
      results[["pscores"]]    = pscores
      results[["trend_breakdates"]] = trend_breakdates
      results[["seasonal_breakdates"]] = seasonal_breakdates
      results[["magnitudes"]] = bf$Magnitude
      results[["time"]]       = bf$Time
      results[["Tt"]]         = bf$output[[iter]]$Tt
      results[["h_param"]]    = h_param
      results[["sig_duration"]] = sig_duration
      results[["dw_test"]]    = dw_test
      prev_sig_duration       = sig_duration
    }
  }
  
  return (results)
}


run_bf_for_df = function (df, coords, start_iter = 1, end_iter, file_pfx) {
  
  fname = paste (file_pfx, "_trend_slopes.csv", sep="")
  if(file.exists(fname)){
    print ("skipping, file exists")
    return()
  }
  
  bf = list()
  trend_slopes = list()
  pscores      = list()
  seasonal_breakdates = list()
  trend_breakdates = list()
  magnitudes   = list()
  time         = list()
  sig_duration = list()
  h_param = list()
  dw_test_stats = list()
  dw_pscores = list()
  
  pscore_slots         = 1:7 # highest poss no. of pscores at h = 1/7
  seasonal_break_slots = 1:4  #  more than we expect?
  trend_break_slots    = 1:6
  
  #browser()
  #dates = as.Date(rownames(df), "EVI_A%Y%j")

  if (missing (end_iter)) {end_iter = length (colnames(data))}

  iters = start_iter:end_iter
  ii = 0
  
  for (i in iters) {
    print ("======")
    print (paste ("sample ", i, sep=""))
    ii = ii + 1
    
    t16     = create16dayts (df[,i], dates)
    results = run_bfast_with_trimmings (t16)
    
    #  now collate the results into their respective lists
    
    bf[[ii]] = results$bf
    
    Tt      = results$Tt
    trend_slopes[[ii]] = Tt[2:length(Tt)] - Tt[1:(length(Tt)-1)]
    
    p = pscore_slots * NA
    p[1:length(results$pscores)] = results$pscores    
    pscores[[ii]] = p
    
    t = trend_break_slots * NA 
    t[1:length(results$trend_breakdates)] = results$trend_breakdates
    trend_breakdates[[ii]] = t
    
    s = seasonal_break_slots * NA 
    t[1:length(results$seasonal_breakdates)] = results$seasonal_breakdates
    seasonal_breakdates[[ii]] = s
    
    magnitudes[[ii]] = results$magnitudes
    
    time[[ii]]       = results$time
    
    h_param[[ii]] = results$h_param
    
    sig_duration[[ii]] = results$sig_duration
    
    dw_test_stats[[ii]] = results$dw_test$statistic
    
    dw_pscores[[ii]] = results$dw_test$p.value
    
    last_t16 = t16
  }

  #browser()
  y_coords = coords$y[[i]]
  x_coords = coords$x[[i]]
  
  trend_slopes_df = t(as.data.frame(trend_slopes))

  row.names(trend_slopes_df) = iters
  output_trend_slopes_df = cbind (x_coords, y_coords, trend_slopes_df, deparse.level= 1)
  colnames (output_trend_slopes_df) = c("X", "Y",paste ("d", dates[2:(length(dates))], sep=""))
  
  print (paste("Writing to ", fname))
  write.csv (output_trend_slopes_df, file = fname, row.names = TRUE)
  
  pscores_df = t(as.data.frame(pscores))
  colnames(pscores_df)  = paste ("P_Value_", pscore_slots, sep = "")
           
  seasonal_breakdates_df = t(as.data.frame(seasonal_breakdates))
  colnames(seasonal_breakdates_df) = paste ("Seasonal_Breakdate_", seasonal_break_slots, sep = "")
            
  trend_breakdates_df    = t(as.data.frame(trend_breakdates))
  colnames(trend_breakdates_df) = paste ("Trend_Breakdate_", trend_break_slots, sep ="")
  
  magnitudes_df = t(as.data.frame(magnitudes))
  colnames(magnitudes_df) = "Magnitude"
  
  time_df = t(as.data.frame(time))
  colnames(time_df) = "Time"
                                 
  h_param_df = t(as.data.frame(h_param))
  colnames(h_param_df)   = "h_Parameter"                                
                                      
  sig_duration_df = t(as.data.frame(sig_duration))
  colnames(sig_duration_df) = "Sig_Duration"
  
  dw_test_stats_df = t(as.data.frame(dw_test_stats))
  colnames(dw_test_stats_df) = "DW_Test_Stat"
  
  dw_pscores_df = t(as.data.frame(dw_pscores))
  colnames(dw_pscores_df) = "DW_P_Value"
  
  all_results_df = cbind (X = x_coords, Y = y_coords, pscores_df, seasonal_breakdates_df,trend_breakdates_df, magnitudes_df, time_df, h_param_df, sig_duration_df, dw_test_stats_df, dw_pscores_df, deparse.level = 1)
  row.names(all_results_df) = iters 
  
  fname = paste (file_pfx, "_all_results.csv", sep="")
  print (paste("Writing to ", fname))
  write.csv (all_results_df, file = fname, row.names = TRUE)
  
  
  all_lists = list()
  
  all_lists[["bf"]]         = bf
  all_lists[["pscores"]]    = pscores
  all_lists[["trend_breakdates"]] = trend_breakdates
  all_lists[["seasonal_breakdates"]] = seasonal_breakdates
  all_lists[["h_param"]]          = h_param
  all_lists[["sig_duration"]]     = sig_duration
  all_lists[["dw_test_stats"]]    = dw_test_stats
  all_lists[["dw_pscores"]]       = dw_pscores
  
  
  return (all_lists) #  poss create a big list of lists to return
  
}

run_bf_for_df_subset = function (df, coords, start_row, end_row, file_pfx = "bfast_res") {
  
  if (missing(coords)) {stop ("coords argument is missing")}
  
  prefix = paste (file_pfx, "_", start_row, "_", end_row, sep="")
  run_bf_for_df (df, coords = coords, start_iter = start_row, end_iter = end_row, file_pfx = prefix)
}
