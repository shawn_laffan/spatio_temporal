#!/bin/bash -login
#PBS -N Tile_03
#PBS -W group_list=ed4
#PBS -P ed4
#PBS -q workq
#PBS -l select=1:ncpus=1:mem=500MB,walltime=100:00:00

ulimit -s unlimited
cd $PBS_O_WORKDIR
echo `pwd`
echo $PBS_O_WORKDIR

source /usr/share/modules/init/bash
export BFAST_INPUT_DATA=/projects/ed4/input_data
export BFAST_USE_HPC=0

Rscript /projects/ed4/repos/spatio_temporal/R/process_EVI_tile.R /projects/ed4/input_data/EVI_1km_2000_2012_03.csv /projects/ed4/res_no_h3/Tile_03

